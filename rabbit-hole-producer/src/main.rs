use std::net::{SocketAddr};

use amiquip::TlsConnector;
use amiquip::{Auth, Connection, ConnectionOptions, ConnectionTuning, Exchange, Publish, Result};
use clap::{App, Arg};
use mio::net::TcpStream;
use url::Url;

fn get_stream(address: &str) -> Result<TcpStream> {
    let socket_addr:SocketAddr = address.parse().unwrap();
    Ok(TcpStream::connect(&socket_addr).unwrap())
}

fn get_tls_connector() -> TlsConnector {
    let native_tls = native_tls::TlsConnector::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();
    return amiquip::TlsConnector::from(native_tls);
}

fn produce(username: &str, password: &str, address: &str, secure: bool) -> Result<()> {
    // Open connection.
    //TODO disable TSL validation for tests :)
    let mut connection = match secure {
        true => {
            println!("using secure connection");
           
            //add amqps as protocol just to allow domain parsing 
            let url = Url::parse(&format!("amqps://{0}", address)).unwrap();
            let domain = url.domain().unwrap();
            let connector = get_tls_connector();
            // print!("domain: {}",domain);
            Connection::open_tls_stream(
                connector,
                domain,
                get_stream(address)?,
                ConnectionOptions::default().auth(Auth::Plain {
                    username: username.to_string(),
                    password: password.to_string(),
                }),
                ConnectionTuning::default(),
            )?
        }
        false => {
            let url = format!("amqp://{0}:{1}@{2}", username, password, address);
            Connection::insecure_open(&url)?
        }
    };

    // Open a channel - None says let the library choose the channel ID.
    let channel = connection.open_channel(None)?;
    // Get a handle to the direct exchange on our channel.
    let exchange = Exchange::direct(&channel);

    for i in 1..300 {
        exchange.publish(Publish::new(
            (format!("hello there {}", i)).as_bytes(),
            "hello",
        ))?;
    }
    // Publish a message to the "hello" queue.
    println!("300 messages successfully published");
    connection.close()
}

fn main() {
    let matches = App::new("rabbitmq example")
        .version(option_env!("CARGO_PKG_VERSION").unwrap_or(""))
        .about("Simple command line producer")
        .arg(
            Arg::with_name("address")
                .short('a')
                .long("address")
                .help("address of rabbitmq")
                .takes_value(true)
                .default_value("localhost:5672"),
        )
        .arg(
            Arg::with_name("secure")
                .short('s')
                .long("secure")
                .help("use amqps of amp")
                .possible_values(&["true", "false"])
                .default_value("false"),
        )
        .arg(
            Arg::with_name("username")
                .long("username")
                .short('u')
                .help("username  for connection")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("password")
                .long("password")
                .short('p')
                .help("password  for connection")
                .takes_value(true),
        )
        .get_matches();

    let address = matches.value_of("address").unwrap();

    let username = matches.value_of("username").unwrap();
    let password = matches.value_of("password").unwrap();
    let secure: bool = matches.value_of("secure") != Some("false");

    produce(username, password, address, secure)
        .map_err(|err| println!("{:?}", err))
        .ok();
}

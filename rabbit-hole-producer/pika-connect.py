import logging
import pika
import ssl

logging.basicConfig(level=logging.INFO)

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

#ssl_options = pika.SSLOptions(ctx, "rabbit-rabbitmq.apps.ocp001.rhlab.ch")
#skupper-edge-rabbitmq.apps.ocp001.rhlab.ch
ssl_options = pika.SSLOptions(ctx, "skupper-edge-rabbitmq.apps.ocp001.rhlab.ch")
credentials = pika.PlainCredentials(
                         'niko',
                         'niko',
                    )

conn_params = pika.ConnectionParameters(port=443,host="skupper-edge-rabbitmq.apps.ocp001.rhlab.ch",
                                        ssl_options=ssl_options,
                                         credentials = credentials)

with pika.BlockingConnection(conn_params) as conn:
    ch = conn.channel()
    ch.queue_declare("foobar")
    ch.basic_publish("", "foobar", "Hello, world!")
    print(ch.basic_get("foobar"))
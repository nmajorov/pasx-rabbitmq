import logging
import pika
import ssl

logging.basicConfig(level=logging.INFO)

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

host ="rabbit-hole-rabbitmq.apps.ocp001.rhlab.ch"
PORT=443
# PORT=5671
ssl_options = pika.SSLOptions(ctx, host)
#skupper-edge-rabbitmq.apps.ocp001.rhlab.ch
#ssl_options = pika.SSLOptions(ctx, "skupper-edge-rabbitmq.apps.ocp001.rhlab.ch")
credentials = pika.PlainCredentials(
                         'niko',
                         'niko',
                    )

conn_params = pika.ConnectionParameters(port=PORT,host=host,
                                        ssl_options=ssl_options,
                                         credentials = credentials)

with pika.BlockingConnection(conn_params) as conn:
    ch = conn.channel()
    ch.queue_declare("foobar")
    ch.basic_publish("", "foobar", "Hello, world!")
    print(ch.basic_get("foobar"))
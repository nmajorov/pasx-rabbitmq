# Client amqp producer in rust

forward port


```
kubectl port-forward --namespace rabbitmq svc/rabbit-hole 5671:5671

```



call producer:

```
cargo run  -- -u niko -p niko -s true -a 127.0.0.1:5671

```

rust producer doesn't work with OpenShift route , as socketaddress is  resolved to router ip and openshift can't send the traffic 
to correct pod.
